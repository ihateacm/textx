# Задание 2.1

## Какие команды определены, есть ли у них параметры?

Определены команды `exit`, `turn`, `move`, `beeper`.
У команды `turn` есть параметр направление, который может быть `left` или `right`.
У команды `beeper` есть параметр действия, который может быть `pick` или `put`.

## Какие ключевые слова есть в языке?

 `begin`, `end`, `exit`, `turn`, `move`, `beeper`
 
## Напишите программу, которая передвигает робота на 2 клетки вперед и на 2 направо

См. `task21.karel`

## Какие соглашения используются для токенизации?

Программа представляет из себя последовательность команд, разделённых пробельнми символами. Некоторые команды используют пробельные символы для аргументов.
