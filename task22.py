from textx import metamodel_from_file
from karel_robot.run import *


class Robot(object):

    def __init__(self):
        self.handlers = {}
        self.register_handlers()

    def _turn(self, s):
        if s.where == 'left':
            turn_left()
        if s.where == 'right':
            turn_right()

    def _move(self, s):
        move()

    def _exit(self, s):
        exit()

    def _beeper(self, s):
        if s.action == 'pick':
            pick_beeper()
        if s.action == 'put':
            put_beeper()

    def _str(self, s):
        self.handlers[s.capitalize()](s)

    def register_handlers(self):
        self.handlers['Turn'] = self._turn
        self.handlers['Move'] = self._move
        self.handlers['Exit'] = self._exit
        self.handlers['Beeper'] = self._beeper
        self.handlers['str'] = self._str

    def process_command(self, s):
        self.handlers[s.__class__.__name__](s)


    def process_statements(self, cs):
        for c in cs:
            self.process_statement(c)


    def process_statement(self, s):
        self.process_command(s)


    def interpret(self, model):

        for s in model.commands:
            self.process_statement(s)

robot_mm = metamodel_from_file('task21.tx')
robot_model = robot_mm.model_from_file("task21.karel")

robot = Robot()
robot.interpret(robot_model)

