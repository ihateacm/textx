from textx import metamodel_from_file
from karel_robot.run import *


class Robot(object):

    def __init__(self):
        self.handlers = {}
        self.evaluators = {}
        self.statements = {}
        self.register_handlers()
        self.register_evaluators()
        self.register_statements()


    def _turn(self, s):
        if s.where == 'left':
            turn_left()
        if s.where == 'right':
            turn_right()

    def _move(self, s):
        move()

    def _exit(self, s):
        exit()

    def _beeper(self, s):
        if s.action == 'pick':
            pick_beeper()
        if s.action == 'put':
            put_beeper()

    def _str_handler(self, s):
        self.handlers[s.capitalize()](s)

    def register_handlers(self):
        self.handlers['Turn'] = self._turn
        self.handlers['Move'] = self._move
        self.handlers['Exit'] = self._exit
        self.handlers['Beeper'] = self._beeper
        self.handlers['str'] = self._str_handler

    def _front_is_treasure(self, s):
        return front_is_treasure()

    def _front_is_blocked(self, s):
        return front_is_blocked()

    def _north(self, s):
        return facing_north()

    def _south(self, s):
        return facing_south()

    def _east(self, s):
        return facing_east()

    def _west(self, s):
        return facing_west()

    def _is_beeper(self, s):
        return beeper_is_present()

    def _or(self, e):
        return self.process_expression(e.left) \
               or self.process_expression(e.right)

    def _and(self, e):
        return self.process_expression(e.left) \
               and self.process_expression(e.right)

    def _not(self, e):
        return not self.process_expression(e.expr)

    def _str_evaluator(self, s):
        return self.evaluators[s](s)

    def register_evaluators(self):
        self.evaluators['front_is_treasure'] = self._front_is_treasure
        self.evaluators['front_is_blocked'] = self._front_is_blocked
        self.evaluators['north'] = self._north
        self.evaluators['south'] = self._south
        self.evaluators['east'] = self._east
        self.evaluators['west'] = self._west
        self.evaluators['is_beeper'] = self._is_beeper
        self.evaluators['Or'] = self._or
        self.evaluators['And'] = self._and
        self.evaluators['Not'] = self._not
        self.evaluators['str'] = self._str_evaluator

    def process_expression(self, e):
        return self.evaluators[e.__class__.__name__](e)

    def process_command(self, s):
        self.handlers[s.__class__.__name__](s)

    def process_statements(self, cs):
        for c in cs:
            self.process_statement(c)

    def _if(self, s):
        if self.process_expression(s.cond):
            self.process_statements(s.commands)
        else:
            self.process_statements(s.else_commands)

    def _while(self, s):
        while self.process_expression(s.cond):
            self.process_statements(s.commands)

    def _for(self, s):
        for i in range(int(s.steps)):
            self.process_statements(s.commands)

    def register_statements(self):
        self.statements['StatementIf'] = self._if
        self.statements['StatementWhile'] = self._while
        self.statements['StatementFor'] = self._for

    def process_statement(self, s):
        if s.__class__.__name__ in self.statements:
            self.statements[s.__class__.__name__](s)
        else:
            self.process_command(s)

    def interpret(self, model):
        for s in model.commands:
            self.process_statement(s)

robot_mm = metamodel_from_file('task23.tx')
robot_model = robot_mm.model_from_file("task23.karel")

robot = Robot()
robot.interpret(robot_model)

